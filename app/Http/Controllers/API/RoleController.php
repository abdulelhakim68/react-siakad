<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\RoleRepositoryInterface;
use Exception;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $roleRepository;
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }
    public function index()
    {
        try {
            //code...
            return response()->json($this->roleRepository->all(['id', 'name', 'guard_name'], array('permissions')), 200);
        } catch (Exception $e) {
            //throw $th;
            return $e->getMessage();
        }
    }
    public function find($id)
    {
        try {
            return response()->json($this->roleRepository->findById($id, ['id', 'name', 'guard_name'], array('permissions'), []), 200);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
