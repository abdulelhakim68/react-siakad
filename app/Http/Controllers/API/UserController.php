<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * userRepository
     *
     * @var mixed
     */
    private $userRepository;
    /**
     * __construct
     *
     * @param  mixed $userRepository
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return response()->json($this->userRepository->all(['id', 'name', 'email'], array('roles')), 200);
    }
    /**
     * find
     *
     * @param  mixed $id
     * @return void
     */
    public function find($id)
    {
        return response()->json($this->userRepository->findById($id, ['id', 'name', 'email'], array('roles'), []), 200);
    }
}
