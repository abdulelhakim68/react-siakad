<?php

namespace App\Http\Controllers\Policies;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use RealRashid\SweetAlert\Facades\Alert;


class RoleActionController extends Controller
{
    /**
     * Assign a role for given User
     *
     * @param Request $request
     * @return void
     */
    public function addRole(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $id     = $request->id;
        $user   = User::find($id);

        /**get role within selected */
        $role = Role::findOrCreate($request->name, 'web');
        $user->assignRole($role);
        $user->save();
        Alert::success('Success', 'Successfully Create new User');
        return back();
    }
}
