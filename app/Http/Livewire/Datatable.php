<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Datatable extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 10;
    public $sortField = 'id';
    public $sortAsc = true;

    public function render()
    {
        return view('livewire.datatable', [
            'users' => User::search($this->search)
                ->orderBy($this->sortField, $this->sortAsc ? 'Asc' : 'Desc')
                ->Paginate($this->perPage),
        ]);
    }
}
