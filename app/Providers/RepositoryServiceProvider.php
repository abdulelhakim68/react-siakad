<?php

namespace App\Providers;

use App\Repository\Contracts\EloquentRepositoryInterface;

/**
 * import dependencies
 */

use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\Eloquent\RoleRepository;

/**
 * import interface dependencies
 */

use App\Repository\RoleRepositoryInterface;
use App\Repository\UserRepositoryInterface;

/**
 * import service laravel
 */

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
