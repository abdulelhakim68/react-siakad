<?php

namespace App\Repository\Eloquent;

use App\Repository\RoleRepositoryInterface;
use \Spatie\Permission\Models\Role;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    protected $model;
    public function __construct(Role $model)
    {
        $this->model = $model;
    }
}
