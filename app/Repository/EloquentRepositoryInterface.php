<?php

namespace App\Repository;

use illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    /**
     * Get all Models.
     * 
     * @param array $columns
     * @param array $relations
     * @return Collection
     */
    public function all(array $columns = ['*'], array $relations = []): Collection;
    /**
     * Get all Trashed models.
     * 
     * @return Collection
     */
    public function allTrashed(): Collection;
    /**
     * find model by Id.
     * 
     * @param int $modelId
     * @param array $columns
     * @param array $relations
     * @param array $appends
     * @return Model
     */
    public function findById(int $modelId, array $columns, array $relations, array $appends): ?Model;
    /**
     * find trashed model by id.
     * 
     * @param int $modelId
     * @return Model
     */
    public function findTrashedById(int $modelId): ?Model;
    /**
     * find only trashed model by id.
     * 
     * @param int $modelId
     * @return Model
     */
    public function findOnlyTrashedById(int $modelId): ?Model;
    /**
     * Create a Model.
     * 
     * @param array $payload
     * @return $model
     */
    public function create(array $payload): ?Model;
    /**
     * Update existing Model.
     * 
     * @param int $modelId
     * @return bool
     */
    public function update(int $modelId, array $payload): bool;
    /**
     * Delete model by Id.
     * 
     * @param int $modelId
     * @return bool
     */
    public function deleteById(int $modelId): bool;
    /**
     * Restore model by Id
     * 
     * @param int $modelId
     * @return bool
     */
    public function restoreById(int $modelId): bool;
    /**
     * Permanently delete model by Id.
     * 
     * @param int $modelId
     * @return  bool
     */
    public function permanentlyDeleteById(int $modelId): bool;
}
