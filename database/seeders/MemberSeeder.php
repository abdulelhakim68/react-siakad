<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Str;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds for Default Member
     *
     * @return void
     */
    public function run()
    {
        /** Reset cached roles and permissions */
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $collection = collect([
            // 'users',
            // 'schedules',
            // 'class',
            'Adress',
            'roles',
            'permissions',
        ]);

        $collection->each(function ($item, $key) {
            /** create permissions group */
            Permission::create(['group' => $item, 'name' => 'create' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'read' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'update' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'delete' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'viewAny' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'view' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'restore' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'forceDelete' . '-' . $item]);
        });

        /**
         * create roles and assign existing permissions
         */
        $role = Role::create(['name' => 'Member']);
        $role->givePermissionTo(Permission::all());
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Member User',
            'email' => 'Member@example.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $user->assignRole($role);
    }
}
