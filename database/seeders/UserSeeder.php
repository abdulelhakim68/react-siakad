<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Str;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $collection = collect([
            'users',
            'roles',
            'permissions',
            'schedules'
            // ... Future Model/ Resource must be declare
        ]);

        $collection->each(function ($item, $key) {
            // create permissions group
            Permission::create(['group' => $item, 'name' => 'create' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'read' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'update' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'delete' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'viewAny' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'view' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'restore' . '-' . $item]);
            Permission::create(['group' => $item, 'name' => 'forceDelete' . '-' . $item]);
        });

        /**
         * create roles and assign existing permissions
         */


        $role1 = Role::create(['name' => 'member']);
        $role1->givePermissionTo(Permission::all());
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Member User',
            'email' => 'test@example.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $user->assignRole($role1);

        $role2 = Role::create(['name' => 'admin']);
        $role2->givePermissionTo(Permission::all());
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Admin User',
            'email' => 'admin@example.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $user->assignRole($role2);

        $role3 = Role::create(['name' => 'super-admin']);
        $role3->givePermissionTo(Permission::all());
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Super-Admin User',
            'email' => 'superadmin@example.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $user->assignRole($role3);
    }
}
