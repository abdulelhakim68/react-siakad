@if ($message = Session::get('success'))
<div class="p-5">
    <div class="flex flex-row h-30 items items-center border-l-4 border-green-500 bg-green-200 rounded-r-xl py-4 px-2">
        <span class="font-bold text-sm text-green-900">{{$message}}</span>
    </div>
</div>
@endif
