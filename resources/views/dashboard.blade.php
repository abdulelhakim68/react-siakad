<x-app-layout>
    <div class="w-4/5 min-h-screen boder-l-2 bg-gray-100">
        {{-- Footer --}}
        <div class="flex justify-between p-5">
            <div class="text-sm text-gray-500 font-bold">
                <a href="#">Privacy</a>
                <a href="#">Policy</a>
            </div>
            <div class="text-sm text-gray-500 font-bold">@ 2020 Workflow, All Right Reserved</div>
        </div>
    </div>
</x-app-layout>
