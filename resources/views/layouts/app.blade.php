<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name ="description" content ="SISTEM INFORMASI AKADEMIK LARAVEL" />
        <meta name ="keywords" content ="Laravel, Siakad, Unisma" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Livewire -->
        <livewire:styles />
    </head>
    <body onload="startTime()" class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            <div class="flex">
                @include('sweetalert::alert')
                @include('layouts.sidebar')
                <!-- Page Content -->
                    {{ $slot }}
                <!-- Page Content -->
            </div>
        </div>
    </body>
    <!-- Livewire Scripts -->
    <livewire:scripts />
</html>
