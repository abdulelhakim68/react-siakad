<div class="w-1/5 bg-green-900 min-h-screen text-white">
    <div class="container mx-auto p-5">
        {{-- Hero User --}}
        <div class="hero">
            <div class="flex flex-row">
                <img class="h-8 w-8" src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg" alt="Workflow">
                <span class="inline-flex ml-5 font-bold text-2xl">Workflow</span>
            </div>
            <div class="flex items-center justify-items-auto my-5 rounded hover:bg-green-200 hover:text-green-700 hover:rounded-full py-2 px-3">
                <div class="flex flex-col">
                    <span class="font-bold text-base">{{ Auth::user()->name }}</span>
                    <span class="text-sm">{{ Auth::user()->email }}</span>
                    <hr class="py-2 mt-2">
                    <span class="text-sm mt-2">
                        <script type='text/javascript'>

                            var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];

                            var date = new Date();

                            var day = date.getDate();

                            var month = date.getMonth();

                            var thisDay = date.getDay(),

                                thisDay = myDays[thisDay];

                            var yy = date.getYear();

                            var year = (yy < 1000) ? yy + 1900 : yy;

                            document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);

                        </script>
                    </span>                        
                    <span id="txt" class="mt-2 text-sm"></span>
                </div>
            </div>
        </div>
        {{-- Search User --}}
        <div class="search">
            <input type="text" class="w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" placeholder="Search">
        </div>
        {{-- Main Menu --}}
        <div class="mt-3">
            {{-- @role('member') --}}
                <div class="flex flex-row items-center justify-items-auto py-2 hover:text-green-700 hover:bg-green-200 px-2 rounded-lg">
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                    <a href="{{route('dashboard')}}" class="inline-flex ml-3">Dashboard</a>
                </div>
            {{-- @endrole --}}

            {{-- @role('admin') --}}
                <div class="flex flex-row items-center justify-items-auto py-2 hover:text-green-700 hover:bg-green-200 px-2 rounded-lg">
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>
                    <a href="{{route('users.index')}}" class="inline-flex ml-3">Users</a>
                </div>
            {{-- @endrole --}}

            {{-- @role('super-admin') --}}
                <div class="flex flex-row items-center justify-items-auto py-2 hover:text-green-700 hover:bg-green-200 px-2 rounded-lg">
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path></svg>
                    <a href="" class="inline-flex ml-3">Role</a>
                </div>

                <div class="flex flex-row items-center justify-items-auto py-2 hover:text-green-700 hover:bg-green-200 px-2 rounded-lg">
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 21h7a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v11m0 5l4.879-4.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242z"></path></svg>
                    <a href="" class="inline-flex ml-3">Permissions</a>
                </div>
            {{-- @endrole --}}

        </div>
        {{-- Account Setting --}}
        <div class="mt-6">
            <header class="text-sm font-bold">Account</header>
            <div class="flex flex-row items-center justify-items-auto py-2">
                <div class="w-2 h-2 bg-blue-600 rounded-xl"></div>
                <a href="{{route('profile')}}" class="inline-flex ml-3">Profile</a>
            </div>
            <div class="flex flex-row items-center justify-items-auto py-2">
                <div class="w-2 h-2 bg-yellow-600 rounded-xl"></div>
                <a href="#" class="inline-flex ml-3">Notification</a>
            </div>
            <div class="flex flex-row items-center justify-items-auto py-2">
                <div class="w-2 h-2 bg-red-600 rounded-xl"></div>
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <a class="inline-flex ml-3" href="{{route('logout')}}"
                        onclick="event.preventDefault();
                        this.closest('form').submit();">
                        {{ __('Logout') }}
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
    }
</script>