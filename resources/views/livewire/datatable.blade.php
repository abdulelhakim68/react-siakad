<div>
    <div class="flex flex-row py-5 gap-x-3">
        <div class="w-1/4">
            <x-label for="search" :value="__('Search')" />
            <x-input wire:model.debounce.500ms="search" id="search" class="block mt-1 w-full" type="text" name="search" :value="old('search')" placeholder="{{__('Search . . . .')}}" autofocus />
        </div>
        <div class="w-1/4">
            <x-label for="sort" :value="__('Sort By Field')" />
            <select wire:model.debounce.500ms="sortField" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50'" name="sort" id="sort">
                <option value="id">Id</option>
                <option value="name">Name</option>
                <option value="email">Email</option>
                <option value="created_at">Register date</option>
            </select>
        </div>
        <div class="w-1/4">
            <x-label for="sort" :value="__('Sort Direction')" />
            <select wire:model="sortAsc" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50'" name="sort" id="sort">
                <option value="1">Ascending</option>
                <option value="0">Descending</option>
            </select>
        </div>
        <div class="w-1/4">
            <x-label for="sort" :value="__('Page Row')" />
            <select wire:model.debounce.500ms="perPage" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50'" name="sort" id="sort">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
    </div>
    <div class="border-b shadow-md border-gray-200 rounded-xl overflow-auto">
        @if ($users->isNotEmpty())
            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-gray-50 border-t">
                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            <input id="check" name="check" type="checkbox" class="h-4 w-4 text-green-600 focus:ring-green-500 border-green-300 rounded">
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Title
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Status
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Role
                        </th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($users as $user)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap text-gray-500">
                            <input id="check" name="check" type="checkbox" class="h-4 w-4 text-green-600 focus:ring-green-500 border-green-300 rounded">
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-gray-500">
                            {{$user->id}}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="flex items-center">
                            <div class="flex-shrink-0 h-10 w-10">
                                <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=4&amp;w=256&amp;h=256&amp;q=60" alt="">
                            </div>
                            <div class="ml-4">
                                <div class="text-sm font-medium text-gray-900">
                                {{$user->name}}
                                </div>
                                <div class="text-sm text-gray-500">
                                {{$user->email}}
                                </div>
                            </div>
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm text-gray-900">Regional Paradigm Technician</div>
                            <div class="text-sm text-gray-500">Optimization</div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                            Active
                            </span>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            @forelse ($user->roles as $roles)
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                    {{ $roles->name }}
                                </span>
                                @empty 
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">No Attached Role</span>
                            @endforelse
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-bold blok">
                            <a href="{{url('dashboard/users/' . $user->id)}}" class="text-green-600 hover:text-indigo-900 ml-3">Show</a>
                            <a href="#" class="text-indigo-600 hover:text-indigo-900 ml-3">Edit</a>
                            <a href="#" class="text-indigo-600 hover:text-indigo-900 ml-3">delete</a>
                        </td>
                    </tr>
                    @endforeach
                    <!-- More items... -->
                </tbody>
            </table>                        
            @else  
            <div class="min-w-full divide-y divide-gray-200">
                <div class="bg-gray-50 border-t flex items-center justify-center py-3">
                    <h1 class="text-red-900 text-md font-bold">Result Not Founds</h1>
                </div>
            </div>
        @endif
    </div>
    <div class="mt-3">
        {{$users->links()}}
    </div>
</div>
