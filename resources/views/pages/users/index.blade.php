<x-app-layout>
    <div class="w-4/5 min-h-screen boder-l-2 bg-gray-100">
        {{-- Header --}}
        <div class="flex items-center justify-between border-b shadow-sm px-5 py-2 bg-white">
            <div>
                <h1 class="text-lg">User List</h1>
            </div>
            <div class="flex gap-x-2">
                <a href="{{route('users.create')}}" class="inline-flex items-center px-4 py-2 rounded-lg bg-green-600 text-sm font-medium text-white hover:text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-green-100 focus:ring-green-500"><span class="font-bold text-sm">Create New User</span></a>
            </div>
        </div>
        {{-- Alert Here --}}
        @include('components.alert')
        {{-- Table Content--}}
        <div class="px-5">
            @livewire('datatable')
        </div>
        {{-- Footer --}}
        <div class="flex justify-between p-5">
            <div class="text-sm text-gray-500 font-bold">
                <a href="#">Privacy</a>
                <a href="#">Policy</a>
            </div>
            <div class="text-sm text-gray-500 font-bold">@ 2020 Workflow, All Right Reserved</div>
        </div>
    </div>
</x-app-layout>
