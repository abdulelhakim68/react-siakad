<x-app-layout>
    <div class="w-4/5 min-h-screen boder-l-2 bg-gray-100">
        <div class="p-5">
        <form action="{{route('assign-role')}}" method="POST">
            @csrf
            <!-- This example requires Tailwind CSS v2.0+ -->
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="px-4 py-5 sm:px-6">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                <span class="font-bold text-green-500">Users Information</span>
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                Personal details of this user.
                <input type="hidden" name="id" value="{{$user->id}}">
                </p>
            </div>
            <div class="border-t border-gray-200">
                <dl>
                <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Full name</span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{$user->name}}
                    </dd>
                </div>
                <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Role User</span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        @forelse ($user->roles as $roles)
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                {{ $roles->name }}
                            </span>
                            @empty 
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">No Attached Role</span>
                        @endforelse
                    </dd>
                </div>
                <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Email address</span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{$user->email}}
                    </dd>
                </div>
                <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Date Registered</span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">                        
                        This user was registered at 
                            <span class="font-bold text-green-700 px-2 bg-green-200 rounded-full">{{$user->created_at->format('H:i:s')}}</span>
                            on
                            <span class="font-bold text-green-700 px-2 bg-green-200 rounded-full">{{$user->created_at->format('d-M-Y')}}</span>
                            <span class="font-bold text-green-700 px-2">{{$user->created_at->diffForHumans()}}</span>
                    </dd>
                </div>
                <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Latest Updated</span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        This user was registered at 
                            <span class="font-bold text-green-700 px-2 bg-green-200 rounded-full">{{$user->updated_at->format('H:i:s')}}</span>
                            on
                            <span class="font-bold text-green-700 px-2 bg-green-200 rounded-full">{{$user->updated_at->format('d-M-Y')}}</span>
                            <span class="font-bold text-green-700 px-2">{{$user->updated_at->diffForHumans()}}</span>
                    </dd>
                </div>
                <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                    <span class="font-bold text-green-500">Permissions Scope of
                    @foreach ($user->roles as $role)
                        {{$role->name}}
                    @endforeach is </span>
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
                            {{-- Check when this user have a role/no --}}
                            @forelse ($user->roles as $role)
                            {{-- Since this User have a role, lets cek that corresponding role have any permission/no  --}}
                                @forelse ($role->permissions as $permission)    
                                    <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                                        <div class="w-0 flex-1 flex items-center">
                                            <!-- Heroicon name: paper-clip -->
                                            <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                                            </svg>
                                            <span class="ml-2 flex-1 w-0 truncate">
                                            {{$permission->name}}
                                            </span>
                                        </div>
                                        <div class="ml-4 flex-shrink-0">
                                            <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">
                                            {{'See Detail'}}
                                            </a>
                                        </div>
                                    </li>
                                    @empty
                                    <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm bg-yellow-100">
                                        <span class="ml-2 flex-1 w-0 truncate text-yellow-500 font-bold">
                                            {{'This User is Granted  to all Permissions'}}
                                        </span>
                                    </li>
                                @endforelse
                            @empty
                            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm bg-red-100">
                                <span class="ml-2 flex-1 w-0 truncate text-red-500 font-bold">
                                    {{ 'This User do not Have Any Role and permissions attachments' }}
                                </span>
                            </li>
                            @endforelse
                        </ul>
                    </dd>
                </div>
                </dl>
            </div>
            </div>
        </div>

        {{-- SHOW SINCE THIS USER DONT HAVE A ROLE --}}
        @forelse ($user->roles as $role)
            
        @empty
            <div class="p-5">
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                        <span class="font-bold Text-lg text-green-500">Assign Role For This User</span> 
                        </h3>
                        <p class="mt-1 max-w-2xl text-sm text-gray-500">
                        Because this user don't have any role attached, you can attach role for this user
                        </p>
                    </div>
                    <div class="border-t border-gray-200">
                        <dl>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                Select Available Role
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                <x-label for="sort" :value="__('Please Select Role')" />
                                <select class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50'" name="name" id="name">
                                    <option value="">Select Role</option>
                                    @foreach ($AvailableRole as $Collection)
                                        <option value="{{$Collection->name}}">{{$Collection->name}}</option>
                                    @endforeach
                                </select>
                                </dd>
                            </div>
                        </dl>
                    </div>
                </div>
                <div class="flex flex-row justify-end items-center py-5 gap-x-3">
                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-bold rounded-lg text-white bg-yellow-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Cancel</button>
                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-bold rounded-lg text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Add</button>
                </div>
            </div>
        @endforelse
        {{-- END SECTION --}}
        </form>

        {{-- Footer --}}
        <div class="flex justify-between p-5">
            <div class="text-sm text-gray-500 font-bold">
                <a href="#">Privacy</a>
                <a href="#">Policy</a>
            </div>
            <div class="text-sm text-gray-500 font-bold">@ 2020 Workflow, All Right Reserved</div>
        </div>
    </div>
</x-app-layout>