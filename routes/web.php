<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Policies\PermissionController;
use App\Http\Controllers\Policies\RoleActionController;
use App\Http\Controllers\Policies\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::prefix('/dashboard')->group(function () {
    Route::get('/profile', ProfileController::class)->middleware('auth')->name('profile');

    Route::post('/user/assign-role', [RoleActionController::class, 'addRole'])->name('assign-role');
    Route::resource('/users', UserController::class)->middleware(['auth']);

    Route::resource('/policies/role', RoleController::class)->middleware(['auth']);
    Route::resource('/policies/permissions', PermissionController::class)->middleware(['auth']);
});
